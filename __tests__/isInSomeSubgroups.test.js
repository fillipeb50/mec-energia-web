import { isInSomeSubgroups } from '../src/utils/validations/form-validations';

describe('isInSomeSubgroups', () => {
  test('Subgrupos Nulos ou Indefinidos', () => {
    const result = isInSomeSubgroups(null, null);
    expect(result).toBe(true);
  });

  test('Subgrupos com Valor Válido', () => {
    const subgroups = [{ min: 10, max: 20 }];
    const result = isInSomeSubgroups(15, subgroups);
    expect(result).toBe(true);
  });

  test('Subgrupos com Valor Inválido e Maior que Máximo', () => {
    const subgroups = [{ min: 10, max: 20 }];
    const result = isInSomeSubgroups(25, subgroups);
    expect(result).toBe('Insira um valor conforme os intervalos ao lado');
  });

  // Adicione mais testes conforme necessário
});